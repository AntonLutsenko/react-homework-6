const initialState = {
	favorites: [],
};

export const favoriteReducer = (state = initialState, action) => {
	switch (action.type) {
		case "RECEIVED_ID_GOODS_IN_FAVORITES": {
			return {
				...state,
				favorites: action.payload,
			};
		}
		default:
			return state;
	}
};
