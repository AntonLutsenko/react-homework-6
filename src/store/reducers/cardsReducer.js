const initialState = {
	cards: [],
};

export const cardReducer = (state = initialState, action) => {
	switch (action.type) {
		case "RECEIVED_ID_GOODS_IN_CARD": {
			return {
				...state,
				cards: action.payload,
			};
		}
		case "CHECKOUT": {
			localStorage.removeItem("card");
			console.log(action.payload, state.cards);
			return { ...state, cards: [] };
		}
		default:
			return state;
	}
};
