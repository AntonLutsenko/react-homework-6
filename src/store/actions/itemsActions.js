import axios from "axios";

export async function getItems() {
	try {
		const { data } = await axios.get("/data.json");
		return data;
	} catch (error) {
		console.log(error);
		return error;
	}
}

export const setItems = () => async (dispatch) => {
	const responce = await getItems();
	if (responce instanceof Error) {
		dispatch({
			type: "GET_ITEMS",
			payload: [],
		});
	} else {
		dispatch({
			type: "GET_ITEMS",
			payload: responce,
		});
	}
};
