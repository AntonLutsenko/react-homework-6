import { useState } from "react";
import { useRef } from "react";

const validateForm = (firstName, lastName) => {
	const errors = {
		firstName: "",
		lastName: "",
		age: "",
		addres: "",
		phone: "",
	};
	if (firstName.length < 4) {
		errors.firstName = "enter you name";
	}
	if (lastName.length < 4) {
		errors.lastName = "enter you name";
	}

	return errors;
};

export const Checkout = () => {
	const [firstNameValue, setFirstNameValue] = useState("");
	const [values, setValues] = useState({
		firstName: "",
		lastName: "",
		age: "",
		addres: "",
		phone: "",
	});
	const [errors, setErrors] = useState({
		firstName: "",
		lastName: "",
		age: "",
		addres: "",
		phone: "",
	});
	const [touched, setTouched] = useState({
		firstName: false,
		lastName: false,
		age: false,
		addres: false,
		phone: false,
	});

	// console.log(firstNameValue);
	// console.log("hi");

	// const handleChange = (e) => {
	// 	console.log(e.target.value);
	// 	// setFirstNameValue(e.target.value);
	// 	setValues({
	// 		...values,
	// 		firstName: e.target.value,
	// 	});
	// };
	// const handlelastNameChange = (e) => {
	// 	setValues({
	// 		...values,
	// 		lastName: e.target.value,
	// 	});
	// };

	const handleChange = (e) => {
		// console.log(e.target.name);
		// console.log(e.target.value);

		const newErrors = validateForm(values.firstName, values.lastName);
		setErrors(newErrors);

		const name = e.target.name;

		// setTouched({
		// 	...touched,
		// 	[name]: true,
		// });
		setValues({
			...values,
			[name]: e.target.value,
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		// console.log("form submit");

		const firstName = values.firstName;
		const lastName = values.lastName;

		const newErrors = validateForm(firstName, lastName);
		setErrors(newErrors);

		// console.log("prev touch", touched);

		// const newTouched = Object.keys(touched).reduce((acc, current) => {
		// 	acc[current] = true;
		// 	return acc;
		// }, {});
		// console.log(`new touched,`, newTouched);

		const newTouched = {};

		Object.keys(touched).forEach((key) => {
			newTouched[key] = true;
		});
		setTouched(newTouched);

		// console.log(`new touched,`, newTouched);

		const hasErrors = Object.values(newErrors).some(
			(errorMsg) => errorMsg.length !== 0
		);
		console.log(hasErrors);
		console.log(firstName);
		console.log(lastName);

		// console.log(newErrors);
		// console.log(firstName);
		// console.log(lastName);
		// console.log(age);
		// console.log(addres);
		// console.log(phone);
	};

	const handleBlur = (e) => {
		const name = e.target.name;
		// const newErrors = validateForm(values.firstName, values.lastName);
		// setErrors(newErrors);
		setTouched({
			...touched,
			[name]: true,
		});
	};

	return (
		<>
			<form onSubmit={handleSubmit}>
				<div>
					<label htmlFor="firstName">First name:</label>
					<input
						onBlur={handleBlur}
						name="firstName"
						value={values.firstName}
						type="text"
						onChange={handleChange}
					></input>
					{touched.firstName && errors.firstName && (
						<div>{errors.firstName}</div>
					)}
				</div>
				<div>
					<label htmlFor="lastName">Last name:</label>

					<input
						onBlur={handleBlur}
						name="lastName"
						value={values.lastName}
						type="text"
						onChange={handleChange}
					></input>
					{touched.lastName && errors.lastName && <div>{errors.lastName}</div>}
				</div>
				<div>
					<label htmlFor="age">Age:</label>
					<input type="number"></input>
				</div>
				<div>
					<label htmlFor="addres">Address:</label>
					<input></input>
				</div>
				<div>
					<label htmlFor="phone">Phone:</label>
					<input></input>
				</div>
				<button type="submit">Checkout</button>
			</form>
		</>
	);
};
