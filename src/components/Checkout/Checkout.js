import { Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { checkout } from "../../store/actions/cardsActions";

export const Checkout = () => {
	const dispatch = useDispatch();

	const SignupSchema = Yup.object().shape({
		firstName: Yup.string()
			.min(2, "Too Short!")
			.max(50, "Too Long!")
			.required("Required"),
		lastName: Yup.string()
			.min(2, "Too Short!")
			.max(50, "Too Long!")
			.required("Required"),
		age: Yup.number().min(0).max(99).integer().positive().required("Required"),
		address: Yup.string()
			.min(10, "Too Short!")
			.max(100, "Too Long!")
			.required("Required"),
		phone: Yup.string()
			.min(10, "Too Short!")
			.max(12, "Too Long!")
			.required("Required"),
	});

	const handleSubmit = (values) => {
		dispatch(checkout(values));
	};

	return (
		<Formik
			initialValues={{
				firstName: "",
				lastName: "",
				age: "",
				address: "",
				phone: "",
			}}
			validationSchema={SignupSchema}
			onSubmit={handleSubmit}
		>
			{(props) => {
				return (
					<Form>
						<div>
							<label htmlFor="firstName">First name:</label>
							<Field name="firstName"></Field>
							{props.touched.firstName && props.errors.firstName && (
								<div>{props.errors.firstName}</div>
							)}
						</div>
						<div>
							<label htmlFor="lastName">Last name:</label>
							<Field name="lastName"></Field>
							{props.touched.lastName && props.errors.lastName && (
								<div>{props.errors.lastName}</div>
							)}
						</div>

						<div>
							<label htmlFor="age">Age:</label>
							<Field name="age"></Field>
							<div>{props.errors.age}</div>
						</div>

						<div>
							<label htmlFor="addres">Address:</label>
							<Field name="address"></Field>
							<div>{props.errors.address}</div>
						</div>

						<div>
							<label htmlFor="phone">Phone:</label>
							<Field name="phone"></Field>
							<div>{props.errors.phone}</div>
						</div>
						<button disabled={props.isSubmitting} type="submit">
							Checkout
						</button>
					</Form>
				);
			}}
		</Formik>
	);
};
