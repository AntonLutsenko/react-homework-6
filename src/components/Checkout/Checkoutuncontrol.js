import { useState } from "react";
import { useRef } from "react";

export const Checkout = () => {
	const firstNameRef = useRef(null);
	const lastNameRef = useRef(null);
	const ageRef = useRef(null);
	const addresRef = useRef(null);
	const phoneRef = useRef(null);
	const [errors, setErrors] = useState({
		firstName: "",
		lastName: "",
		age: "",
		addres: "",
		phone: "",
	});

	const validateForm = (firstName) => {
		const errors = {
			firstName: "",
			lastName: "",
			age: "",
			addres: "",
			phone: "",
		};
		if (firstName.length < 4) {
			console.log("hi");
			errors.firstName = "enter you name";
		}
		return errors;
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		// console.log("form submit");

		const firstName = firstNameRef.current.value;
		const lastName = lastNameRef.current.value;
		const age = ageRef.current.value;
		const addres = addresRef.current.value;
		const phone = phoneRef.current.value;
		const newErrors = validateForm(firstName);

		setErrors(newErrors);

		const hasErrors = Object.values(newErrors).some(
			(errorMsg) => errorMsg.length !== 0
		);
		console.log(hasErrors);
		console.log(newErrors);
		// console.log(firstName);
		// console.log(lastName);
		// console.log(age);
		// console.log(addres);
		// console.log(phone);
	};

	return (
		<>
			<form onSubmit={handleSubmit}>
				<div>
					<label htmlFor="firstName">First name:</label>
					<input type="text" ref={firstNameRef}></input>
					{errors.firstName && <div>{errors.firstName}</div>}
				</div>
				<div>
					<label htmlFor="lastName">Last name:</label>
					<input type="text" ref={lastNameRef}></input>
				</div>
				<div>
					<label htmlFor="age">Age:</label>
					<input type="number" ref={ageRef}></input>
				</div>
				<div>
					<label htmlFor="addres">Address:</label>
					<input ref={addresRef}></input>
				</div>
				<div>
					<label htmlFor="phone">Phone:</label>
					<input ref={phoneRef}></input>
				</div>
				<button type="submit">Checkout</button>
			</form>
		</>
	);
};
