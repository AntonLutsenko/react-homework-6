// import React, { useState, useEffect } from "react";
// import { ProductCardContext } from "./ProductCardContext";
// import axios from "axios";

// const ProductCardConteiner = (props) => {
// 	const [items, setItems] = useState([]);
// 	const [card, setCard] = useState([]);
// 	const [show, showModal] = useState(false);
// 	const [modal, modalForm] = useState();
// 	const [favorite, setFavorite] = useState([]);

// 	useEffect(() => {
// 		axios("/data.json").then((res) => {
// 			setItems(res.data);
// 		});

// 		if (!localStorage.getItem("card")) {
// 			localStorage.setItem("card", "[]");
// 		} else {
// 			setCard(JSON.parse(localStorage["card"]));
// 		}

// 		if (localStorage.getItem("favorites") !== null) {
// 			// console.log("hi");
// 			setFavorite(JSON.parse(localStorage["favorites"]));
// 			// let a = () => {
// 			// 	console.log(items);
// 			// 	console.log(favorite);
// 			// 	// items.map((el) => {
// 			// 	// 	console.log(el);
// 			// 	// });
// 			// };
// 			// a();
// 		}
// 	}, []);
// 	useEffect(() => {
// 		const newItems = items.map((element) => {
// 			favorite.includes(element)
// 				? (element.isFavorite = true)
// 				: (element.isFavorite = false);
// 			return element;
// 		});

// 		setItems(newItems);
// 	}, [favorite, card]);

// 	const addToFavorite = (element) => {
// 		if (favorite.includes(element)) {
// 			const newFavorites = favorite.filter((item) => item !== element);
// 			setFavorite(newFavorites);
// 		} else {
// 			setFavorite((arr) => [...arr, element]);
// 		}
// 	};

// 	useEffect(() => {
// 		localStorage.setItem("favorites", JSON.stringify(favorite));
// 	}, [favorite]);

// 	const openModal = (id) => {
// 		console.log("hi");
// 		showModal(true);
// 		const dataForModal = card.find((item) => item.id === id);
// 		modalForm(dataForModal);
// 	};

// 	const closeModal = () => {
// 		// console.log("hi");
// 		showModal(false);
// 	};

// 	const addToCard = (id) => {
// 		showModal(true);
// 		let dataForModal = items.find((item) => item.id === id);
// 		// console.log(dataForModal);
// 		modalForm((dataForModal = { ...dataForModal, idForDelete: id }));
// 		card.push(dataForModal);
// 		// console.log(card);
// 		localStorage.setItem("card", JSON.stringify(card));
// 	};

// 	// const clearCard = () => {
// 	// 	// localStorage.removeItem("card");
// 	// 	console.log("hi");
// 	// 	// setCard([]);
// 	// };

// 	const hendleDelete = (id) => {
// 		// console.log("hi");
// 		console.log(id);
// 		// console.log(id);
// 		// console.log(element.idForDelete);

// 		// const newCard = card.filter((element) => console.log(element.idForDelete));
// 		// console.log(newCard);

// 		const newCard = card.filter((element) => {
// 			if (element.idForDelete === id) {
// 				// console.log(id);
// 				// console.log(element.idForDelete);
// 				// setCard(newCard);
// 				return false;
// 			} else {
// 				return true;
// 			}
// 		});
// 		// console.log(newCard);
// 		setCard(newCard);
// 		localStorage.setItem("card", JSON.stringify(newCard));
// 		closeModal();
// 	};

// 	// useEffect(() => {
// 	// 	// if (localStorage.getItem("favorites") !== null) {
// 	// 	// console.log(favorite);
// 	// 	const newItems = items.map((element) => {
// 	// 		// console.log(element);
// 	// 		console.log(favorite.includes(element));
// 	// 		// console.log(element);
// 	// 		favorite.includes(element)
// 	// 			? (element.isFavorite = true)
// 	// 			: (element.isFavorite = false);
// 	// 		return element;
// 	// 	});

// 	// 	setItems(newItems);
// 	// 	// console.log(newItems);
// 	// 	// console.log(items);
// 	// 	// }
// 	// }, [favorite]);

// 	const { children } = props;

// 	return (
// 		<ProductCardContext.Provider
// 			value={{
// 				items,
// 				card: card,
// 				setCard: setCard,
// 				// clearCard: clearCard,
// 				onDelete: hendleDelete,
// 				addToCard: addToCard,
// 				closeModal: closeModal,
// 				addToFavorite: addToFavorite,
// 				openModal: openModal,
// 				show,
// 				modal,
// 				favorite,
// 			}}
// 		>
// 			{children}
// 		</ProductCardContext.Provider>
// 	);
// };

// export default ProductCardConteiner;
