import "./Reset.css";
import "./App.scss";

import { BrowserRouter } from "react-router-dom";
import MarcetPlace from "./components/MarcetPlace/MarcetPlace";

const App = () => {
	return (
		<BrowserRouter>
			<MarcetPlace />
		</BrowserRouter>
	);
};

export default App;
